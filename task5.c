#include <stdio.h>

int main(int argc, char *argv[], char *envp[]) {
    FILE *input_file;
    FILE *output_file;

    char ch;

    if (argc != 3) {
        printf("Please provide enough arguments\n");
        return 1;
    }

    input_file = fopen(argv[1], "r");
    if (input_file == NULL) {
        printf("Error opening the input file\n");
        return 1;
    }

    output_file = fopen(argv[2], "w");
    if (output_file == NULL) {
        printf("Error opening the output file\n");
        fclose(input_file);
        return 1;
    }

    ch = getc(input_file);
    while (ch != EOF) {
        putc(ch, output_file);
        ch = getc(input_file);
    }

    fclose(input_file);
    fclose(output_file);
    return 0;
}