#include <stdio.h>
#include <dirent.h>
#include <errno.h>

int main(int argc, char *argv[], char *envp[]) {
    DIR *dir1;
    DIR *dir2;
    struct dirent *dir;

    if (argc != 2) {
        printf("Please provide enough arguments\n");
        return 1;
    }

    dir1= opendir(".");
    if (dir1 != NULL) {
        printf("Current directory:\n");
        while (dir = readdir(dir1))
            printf("%s\n", dir->d_name);
    } else {
        printf("Error opening the current directory %s\n");
        return 1;
    }
    closedir(dir1);

    dir2 = opendir(argv[1]);
    if (dir2 != NULL) {
        printf("Directory %s:\n", argv[1]);
        while (dir = readdir(dir2))
            printf("%s\n", dir->d_name);
    } else if (ENOENT == errno) {
        printf("Directory does not exist\n");
        return 1;
    } else {
        printf("Error opening the directory %s\n", argv[1]);
        return 1;
    }
    closedir(dir2);
    return 0;
}
