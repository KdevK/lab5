#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *envp[]) {
    FILE *file;
    int N;
    char ch;

    if (argc != 3) {
        printf("Please provide the filename and the number of lines");
        return 1;
    }

    N = atoi(argv[2]);

    file = fopen(argv[1], "r");
    if (file == NULL) {
        printf("Error opening the file");
        return 1;
    }

    ch = getc(file);
    if (N == 0) {
        while (ch != EOF) {
            putchar(ch);
            ch = getc(file);
        }
    }
    else {
        while (ch != EOF) {
            for (int i = 0; i < N; i++) {
                while (ch != '\n') {
                    putchar(ch);
                    ch = getc(file);
                    if (ch == EOF) break;
                }
                ch = getc(file);
                printf("\n");
                if (ch == EOF) break;
            }
            if (ch == EOF) break;
            printf("Press enter to continue");
            getchar();
        }
    }
    fclose(file);
    return 0;
}
