#include <stdio.h>

int main(int argc, char *argv[], char *envp[]) {
    if (argc != 2) {
        printf("Please provide the output file");
        return 1;
    }

    int ch;
    FILE *file;
    file = fopen(argv[1], "w");

    if (file == NULL) {
        printf("Error opening the file");
        return 1;
    }

    do {
        ch = getchar();
        putc(ch, file);
    } while (ch != 6);

    fclose(file);
    return 0;
}
